This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Work on progress... Live demo: [demo](http://jankowskiszymon.pl/game/) !!! This is EARLY VERSION OF APP. I'm coding it since 22.10.2019. THERE IS NO MOBILE VERSION.

## 1. Made with

- [ ] [React.js](https://reactjs.org/)
- [ ] [Mountain Dusk Parallax background](https://ansimuz.itch.io/mountain-dusk-parallax-background)
- [ ] [parallax.js](https://matthew.wagerfield.com/parallax/)

## 2 Game 

This is a simple clicker game, but with complex mechanics of getting items and experience.

![photoOfGame](https://gitlab.com/petergrynn/lootclicker/raw/master/readme/game.png)

## 3 Mechanics 

### 3.1 Exp mechanics 

```javascript 

// experience from monsters depending on the level 
const getExp = (lvl) => {
    let exp = getRandomInt(lvl*5,lvl*6); // randomness for more interesting mechanics
    exp = round(exp, 0)+1; 
    exp =  Math.pow(exp,1.4); // power helps to combine level and playing time (1.4)
    return exp;
}

// expierence to lvl up depending on the level 
const getExp2Lvl = (lvl) => {
    let exp = getRandomInt(lvl*5,lvl*6); // randomness for more interesting mechanics
    exp = round(exp, 0)+1; 
    exp = Math.pow(exp,2.4); // power helps to combine level and playing time (2.4)
    return exp;
}

```

And this formulas gives us interesting graph 

![graph1](https://gitlab.com/petergrynn/lootclicker/raw/master/readme/graph1.png)

### 3.2 Dmg mechanism

### 3.3 Loot mechanism

### 3.4 Eq mechanism 

## 4. To do 

- [ ] Add life and exp index
- [ ] Create loot algorithm
- [ ] Make some space for items
- [ ] Make simple Eq and combine it with dmg mechanism
- [ ] Rewrite mechanics of game from useState to Reducers

## 5. Information 

### 5.1 Licence

This project is licensed under the terms of the  [MIT](http://www.opensource.org/licenses/mit-license.php) License. Enjoy!

## 5.2 Authors 

Szymon Jankowski: [facebook](https://www.facebook.com/petergrynn) | [linkedin](https://www.linkedin.com/in/szymon-j-9947b3142/) | [gitlab](https://gitlab.com/petergrynn)
