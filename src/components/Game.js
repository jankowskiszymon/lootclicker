import React, { useState } from 'react';
import {
    GameArea
} from './gameComponents/gameStyle';
import Monster from './gameComponents/Monster';
import Stats from './gameComponents/Stats';
import { Monsters } from './gameComponents/Mobs';

const round = (n, k) => {
    let factor = Math.pow(10, k);
    return Math.round(n*factor)/factor;
}

const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

const getExp = (lvl) => {
    let exp = getRandomInt(lvl*5,lvl*6);
    exp =  Math.pow(exp,1.4);
    exp = round(exp, 0)+1;
    return exp;
}

export const getExp2Lvl = (lvl) => {
    let exp = lvl*5;
    exp = Math.pow(exp,2.22);
    exp = round(exp, 0)+1;
    return exp;
}

let ar = [];

for (let i = 0; i < 100; i++) {
    ar[i] = getExp2Lvl(i)/getExp(i);
}
console.log(ar);
const Game = () => {

    const [hero, setHero]= useState([{
        name: "hero",
        lvl: 1,
        dmg: 18,
        exp: 0
    }])

    const [mob, setMob] = useState([{
        name: "bat",
        lvl: 1,
        maxHp: 100,
        hp: 24, 
        exp: getExp(1),
        numer: 0
    }]);
    
    const [stats, setStats] = useState([{
        clicks: 0,
        dmg: 0,
        kills: 0
    }])

    const click = () => {
        setStats([{
            clicks: stats[0].clicks + 1,
            dmg: stats[0].dmg + hero[0].dmg,
            kills: stats[0].kills 
        }]) 
        
        if(mob[0].hp <= hero[0].dmg){
            newMonster();
            setStats([{
                clicks: stats[0].clicks + 1,
                dmg: stats[0].dmg + hero[0].dmg,
                kills: stats[0].kills + 1
            }]) 
            giveExp();
            if (hero[0].exp > getExp2Lvl(hero[0].lvl)){
                lvlUp();
            }
            return;
        } else {
            hitMonster()
            if (hero[0].exp > getExp2Lvl(hero[0].lvl)){
                lvlUp();
            }
            
        }

        console.log(mob)      
    }

    const newMonster = () => {

        let n = getRandomInt(0,Monsters.normal.length);
        let hpRandom = getRandomInt(0,hero[0].lvl+3)

        setMob([{
            name: Monsters.normal[n],
            lvl: hero[0].lvl,
            maxHp: hero[0].lvl*13+hpRandom,
            hp: hero[0].lvl*13+hpRandom,
            exp: getExp(hero[0].lvl),
            numer: n
        }])
    }

    const hitMonster = () => {
        let m = mob[0];
        setMob([{
            name: m.name,
            lvl: m.lvl,
            maxHp: m.maxHp,
            hp: m.hp - hero[0].dmg,
            exp: m.exp,
            numer: m.numer
        }])
    }

    const lvlUp = ( ) => {
        setHero([{
            name: hero[0].name,
            lvl: hero[0].lvl+1,
            dmg: hero[0].dmg,
            exp: 0
        }])
    }

    const giveExp = () => {
        setHero([{
            name: hero[0].name,
            lvl: hero[0].lvl,
            dmg: hero[0].dmg,
            exp: hero[0].exp + mob[0].exp
        }])
    }

    return (
        <GameArea onClick={click}>
            <Monster mob={mob[0]}/>
            <Stats hero={hero[0]} stats={stats[0]}/>
        </GameArea>
    )
}

export default Game;