import React from 'react';
import styled from 'styled-components';
import { 
    HeroStats,
    GameStats
} from './gameStyle';


const StyledDiv = styled.div`
    width: 50%;
    box-sizing: border-box;
`

const Stats = ({hero, stats}) => {

    return (
        <StyledDiv>
            <HeroStats name={hero.name} lvl={hero.lvl} dmg={hero.dmg} exp={hero.exp}/>
            <GameStats clicks={stats.clicks} dmg={stats.dmg} kills={stats.kills}/>
        </StyledDiv>
    )
}

export default Stats;