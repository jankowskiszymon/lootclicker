import React, { useState } from 'react';
import { 
    MonsterArea,
    Mob, 
    MobContainer,
    MonsterStats
} from './gameStyle';
import styled from 'styled-components';

const importAll = require =>
  require.keys().reduce((acc, next) => {
    acc[next.replace("./", "")] = require(next);
    return acc;
  }, {});

const images = importAll(
  require.context("../../img/monsters", false, /\.(png)$/)
);

const ProgressValue = styled.div`
  background-color: #d93d04;
  width: ${props => props.value ? `${props.value}px`: '0px'};
  height: 100%;
  box-sizing: border-box;
  border-radius: 1px;
  margin-top: -19px;
  span{
    color: black;
    width: 200px;
  }
`

const ProgressDiv = styled.div`
  height: 20px;
  width: 200px;
  border: 2px solid black;
  margin: 0 auto;
  box-sizing: border-box;
  border-radius: 1px;
  
`

const Progress = ({value, max}) => {
  let width = 200*(value/max)-4;
  console.log(width);
  return (
    <ProgressDiv>
      <span>{value} / {max}</span>
      <ProgressValue value={width}></ProgressValue>
    </ProgressDiv>
  )
}

const img = Object.values(images);

const Monster = ({mob}) => {
    
    return (
        <MonsterArea>
            <MonsterStats name={mob.name} lvl={mob.lvl} hp={mob.hp} exp={mob.exp}/>
            <MobContainer>
              <Mob src={img[mob.numer]} />
            </MobContainer>
            <Progress value={mob.hp} max={mob.maxHp}/>
        </MonsterArea>
    )
}

export default Monster;