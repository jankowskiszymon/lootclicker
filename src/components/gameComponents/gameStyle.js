import styled from 'styled-components';
import React, {useState,useEffect} from 'react';
import { getExp2Lvl } from '../Game';

export const GameArea = styled.div`
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    z-index: 2;
    height: 400px;
    width: 70vw;
    display: inline-flex;
`
export const Mob = styled.img`
    height: 250px;
    :hover{
        height: 252px;
        cursor: pointer;
    }
    :active{
        height: 248px;
    }
`
export const MobContainer = styled.div`
    height: 260px;
`

export const MonsterArea = styled.div`
    width: 50%;
    text-align: center;
    display: block;
`
const Stats = styled.div`
    width: 100%;
`
export const MonsterStats = ({name, lvl, hp, exp}) => {

    return (
        <Stats>
            <h1>{name}</h1>
            <p>Lvl: {lvl} Hp: {hp} Exp: {exp}</p>
        </Stats>
    )
}

const Stat = styled.div`
    text-align: center;
    height: 40%;
`

const ProgressDiv = styled.div`
    margin: 0 auto;
    border: 2px solid black;
    width: 270px;
    height: 20px;
    border-radius: 5px;
`

const Exp = styled.div`
    width: ${props => props.exp ? `${props.exp}px`: `100%`};
    background-color: yellow;
    height: 20px;
    margin-top: -19px;
`

const Progress = ({exp,lvl}) => {

    return (
        <ProgressDiv>
            {exp} / {getExp2Lvl(lvl)}
            <Exp exp={270*exp/getExp2Lvl(lvl)-5}></Exp>
        </ProgressDiv>
    )
}

export const HeroStats = ({name, lvl,dmg, exp}) => {

    return (
        <Stat>
            <h1>{name}</h1>
            <p>Lvl: {lvl} | Dmg: {dmg} </p>
            <Progress exp={exp} lvl={lvl}/>
        </Stat>
    )
}
export const GameStats = ({clicks, dmg, kills}) => {
    const [time, setTime] = useState([{
        h: 0,
        m: 0,
        s: 0
    }])

    useEffect(()=>{
        timer();
        setInterval(timer,1000)
    }, [])

    const timer = () => {
        if (time[0].s > 59) {
            setTime([{
                h: time[0].h,
                m: time[0].m++,
                s: 0
            }])
        } if (time[0].m > 59) {
            setTime([{
                h: time[0].h++,
                m: 0,
                s: 0
            }])
        } else {
            setTime([{
                h: time[0].h,
                m: time[0].m,
                s: time[0].s++
            }])
        }
    }

    const displayTime = (h,m,s) => {
        let [min,sek] = [m,s];
        if (s < 10) sek = `0${s}`;
        if (m < 10) min = `0${m}`;

        return `${h}:${min}:${sek}`;
    }

    return (
        <Stat>
            <h1>Statistics</h1>
            <p>Time: {displayTime(time[0].h,time[0].m,time[0].s)} | Clicks: {clicks} | Dmg: {dmg} | Kills: {kills}</p>
        </Stat>
    )
}