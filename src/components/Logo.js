import React from 'react';
import styled from 'styled-components';

const LogoArea = styled.div`
    position: absolute;
    top: 10%;
    left: 50%;
    transform: translate(-50%,-50%);
    z-index: 2;
    font-size: 80px;
    
`

const Logo = ({title, version}) => {

    return (
        <LogoArea>
                {title}
        </LogoArea>
    )
}

export default Logo;