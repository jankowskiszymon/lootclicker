import React, { useEffect } from 'react';
import bg1 from '../img/background/bg1.png';
import bg2 from '../img/background/bg2.png';
import bg3 from '../img/background/bg3.png';
import bg4 from '../img/background/bg4.png';
import bg5 from '../img/background/bg5.png';
import styled from 'styled-components';
import Parallax from 'parallax-js';

const Scene = styled.div`
    z-index: 1;
    p{
        margin-top: 100vh;
        text-align: center;
        color: black;
    }
`

const Bg1 = styled.div`
    background-image: url(${bg1});
    background-size: cover;
    height: 100vh;
    position: absolute;
    width: 100vw;
    
`
const Bg2 = styled.div`
    background-image: url(${bg2});
    background-size: cover;
    height: 100vh;
    position: absolute;
    width: 100vw;    
`
const Bg3 = styled.div`
    background-image: url(${bg3});
    background-size: cover;
    height: 100vh;
    position: absolute;
    width: 100vw;    
`
const Bg4 = styled.div`
    background-image: url(${bg4});
    background-size: cover;
    height: 100vh;
    position: absolute;
    width: 100vw;    
`
const Bg5 = styled.div`
    background-image: url(${bg5});
    background-size: cover;
    height: 100vh;
    position: absolute;
    width: 100vw;    
`

const Background = ({version}) =>{

    useEffect(()=>{
        var scene = document.getElementById('scene');
        // eslint-disable-next-line
        var parallaxInstance = new Parallax(scene);
    }, [])

    return (
        <Scene id="scene">
            <Bg1 data-depth="-0.3"></Bg1>
            <Bg2 data-depth="-0.15"></Bg2>
            <Bg3 data-depth="0"></Bg3>
            <Bg4 data-depth="0.15">v: {version}</Bg4>
            <Bg5 data-depth="0.3"><p>Author: Szymon Jankowski</p></Bg5>
        </Scene>
    )
}

export default Background; 