import React, { useEffect } from 'react';
import Background from './components/Background';
import Logo from './components/Logo';
import Game from './components/Game';


const App = () => {

  return (
    <div id="Scene">
      <Logo title="LootClicker"/>
      <Game />
      <Background version="0.11"/>
      
    </div>
  )
};

export default App;
